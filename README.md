### Introduction

This is an example of a Blog API using microservices and API gateway patterns. 

# Demo

The project is deployed here: http://yunke.3utilities.com:3000/api

and the existing end points are:

* /blog/posts

* /comments

* /tags

* /users

There is also a postman collection in the repository to tests the requests.

# API Gateway

Single point of entry of the API. Here we will have common code, but nothing domain related.
The job of the API Gateway will be:

* Forward requests to the corresponding service. This way the client works exactly the same as it would do against a single REST API.

* Cache responses. A cache system is in place to cache responses from the microservices and that way skip making unnecessary requests and making the API quicker (not implemented yet).

* Data aggregation. Prevent the client from making multiple requests to multiple services when needed. For example, to get a blog post by ID, we will include the last 10 comments and the tags in the response to the client, so it will receive all the information to render the blog post (not implemented yet)


### TODO:

- Authentication

# Microservices

Each service has a single model and controller.

 * Blog posts API
 
It has a CRUD Controller and tests for each method. createdBy attribute will have an _id set manually and it will be the user _id.

 * Comments API
 
It has a CRUD Controller and tests for each method. The parentItemId will be the blog post _id. createdBy attribute will have an _id set manually and it will be the user _id.

 * Tags
 
It has a CRUD Controller and tests for each method. The _id will be set manually and it will be the blog post _id.

 * Users
 
It has a CRUD Controller and tests for each method. Will be used to create blog posts and comments.

# API Generator

This is a console util to create a skeleton microservice in NodeJS as starting point. It will have some stuff to avoid doing it every time for each service or copy/pasting it:

 * Node app with npm dependencies (express, validators, etc)
 
 * Docker configuration (Dockerfile and docker-compose)
 
 * Mongo and Node config (connections)
 
 * Controller and model structure with validation (although missing actual model which has to be populated later on)
 
 * Controller tests (testdata also needs to be populated)
 

This is an example config:

```
{
  "name": "comments-api",
  "description": "Basic commenting API",
  "author": "Josï¿½ Ramï¿½n Blanco Rey",
  "title": "Comments API",
  "router": {
    "prefix": "/api",
    "entity": "/comments"
  },
  "controller": {
    "name": "CommentsController",
    "mandatoryPostParams": ["parentItemId", "createdBy", "body"],
    "methods": {
      "get": "getComments",
      "getById": "getCommentById",
      "post": "crateNewComment",
      "put": "updateComment",
      "delete": "deleteComment"
    }
  },
  "mongo": {
    "model": "CommentModel"
  }
}
```

### Create a new project:

 * Run de command from the root of the api-generator project:

```npm start path/to/config.json```

The project will be generated in the output/ folder.


### Next steps:

* Copy files from output/ to your new project's folder

* Populate missing content in the following files:

- test/testedata.js

We need test data to run our controller tests

Example:

```
module.exports = {
    "entitySample": {
      "__v": 0,
      "title": "my title",
      "body": "This is my first blog entry",
      "createdAt": "2017-08-04T17:06:31.274Z",
      "createdBy": {
        "userId": "someUserId",
        "name": "someUserName"
      },
      "_id": "5984a997f3bf94001cb97bec"
    },
    "updatedEntitySample": {
        "__v": 0,
        "title": "my UPDATED title",
        "body": "This is my first UPDATED entry",
        "createdAt": "2017-08-04T17:06:31.274Z",
        "createdBy": {
            "userId": "someUserId",
            "name": "someUserName"
        },
        "_id": "5984a997f3bf94001cb97bec"
    },
    "invalidIds": ["invalidId", 3245345, null],
    "invalidPostData": [
      {
        "body": "missing title",
        "createdBy": {
          "userId": "someUserId",
          "name": "someUserName"
        }
      },
      {
        "title": "missing body",
        "createdBy": {
          "userId": "someUserId",
          "name": "someUserName"
        }
      },
      {
        "title": "333333",
        "body": "missing createdBy"
      },
      {
        "title": 235645267,
        "body": "invalid title",
        "createdBy": {
          "userId": "someUserId",
          "name": "someUserName"
        }
      },
      {
        "title": "",
        "body": "empty title",
        "createdBy": {
          "userId": "someUserId",
          "name": "someUserName"
        }
      },
      {
        "title": "invalid body",
        "body": 56256,
        "createdBy": {
          "userId": "someUserId",
          "name": "someUserName"
        }
      },
      {
        "title": "empty body",
        "body": "",
        "createdBy": {
          "userId": "someUserId",
          "name": "someUserName"
        }
      }
    ],
    "invalidPutData": [
        {
            "title": 235645267,
            "body": "invalid title"
        },
        {
            "title": "",
            "body": "empty title"
        },
        {
            "title": "invalid body",
            "body": 56256
        },
        {
            "title": "empty body",
            "body": ""
        }
    ]
  };

```

- src/validator/ParamValidator.js

We need the expected json body to validate POST and PUT requests

Example:

```
{
    "title": "",
    "body": "",
    "createdBy": {
        "userId": "",
        "name": "",
        "photoUrl": ""
    }
}
```

- src/controller/Controller.js

We need the mandatory params array for POST requests

Example:    

```
let isValid = ParamValidator.validateBody(
            req.body, // data
            ['title', 'createdBy', 'body'] // mandatory params
        );
```

and run the following commands from the root of your new project:

* Install dependencies

```npm install```

- if it doesn install dev dependencies, try:

```npm install --only=dev```

* Build (tests run con code compiled from ES2015 to ES5)

```npm run build```

- ES5 code will be in the dist/ folder

* Start docker containers

```docker-compose up --build```

* Run tests

```npm test```

* Add more code to your project

### TODO:

* Populate the data that know requires to be done after generation

* Allow output dir as command parameter

* Validate config files 

* Tests