'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _config = require('config');

var _config2 = _interopRequireDefault(_config);

var _ApiGatewayController2 = require('./ApiGatewayController');

var _ApiGatewayController3 = _interopRequireDefault(_ApiGatewayController2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _possibleConstructorReturn(self, call) { if (!self) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return call && (typeof call === "object" || typeof call === "function") ? call : self; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

/**
 * Users controller
 * (will be used by the router)
 */
var UsersController = function (_ApiGatewayController) {
  _inherits(UsersController, _ApiGatewayController);

  /**
   * Constructor
   * (sets the correct api url)
   */
  function UsersController() {
    _classCallCheck(this, UsersController);

    return _possibleConstructorReturn(this, (UsersController.__proto__ || Object.getPrototypeOf(UsersController)).call(this, _config2.default.get('api.users')));
  }

  return UsersController;
}(_ApiGatewayController3.default);

exports.default = UsersController;