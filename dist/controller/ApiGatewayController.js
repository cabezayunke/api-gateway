'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _axios = require('axios');

var _axios2 = _interopRequireDefault(_axios);

var _CacheClient = require('../cache/CacheClient');

var _CacheClient2 = _interopRequireDefault(_CacheClient);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * Blog post controller
 * (will be used by the router)
 */
var ApiGatewayController = function () {

    /**
     * Constructor
     * @param baseUrl, set from child class
     */
    function ApiGatewayController(baseUrl) {
        _classCallCheck(this, ApiGatewayController);

        this.baseUrl = baseUrl;
        this.cacheClient = new _CacheClient2.default();
    }

    /**
     * Forward request to corresponding service
     *
     * @param req
     * @param res
     * @param next
     */


    _createClass(ApiGatewayController, [{
        key: 'forwardRequest',
        value: function forwardRequest(req, res, next) {
            var _this = this;

            // prepare options
            var options = {
                method: req.method || 'get',
                url: this.baseUrl + req.originalUrl,
                responseType: 'application/json'
            };
            if (req.method === 'POST' || req.method === 'PUT') {
                options.data = req.body || {};
            }
            console.log(options);
            // check cache
            if (req.method === 'get') {
                var cachedItem = this.cacheClient.getCachedItem(options.url);
                if (cachedItem) {
                    res.status(apiResponse.status).send(apiResponse.data);
                    return;
                }
            }

            // send request
            (0, _axios2.default)(options).then(function (apiResponse) {
                console.log(apiResponse.data);
                // save item to cache
                if (req.method === 'get') {
                    _this.cacheClient.saveItem(options.url, apiResponse.data);
                }
                res.status(apiResponse.status).send(apiResponse.data);
            }).catch(function (error) {
                console.warn(error);
                return next(error);
            });
        }
    }]);

    return ApiGatewayController;
}();

exports.default = ApiGatewayController;