"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * Cache client
 * TODO: implement with Redis
 */
var CacheClient = function () {
  function CacheClient() {
    _classCallCheck(this, CacheClient);
  }

  _createClass(CacheClient, [{
    key: "getCachedItem",


    /**
     * Gets item from cache
     *
     * @param key
     * @returns {boolean}
     */
    value: function getCachedItem(key) {
      return false;
    }

    /**
     * Saves item to cache
     *
     * @param key
     * @param data
     * @returns {boolean}
     */

  }, {
    key: "saveItem",
    value: function saveItem(key, data) {
      return false;
    }
  }]);

  return CacheClient;
}();

exports.default = CacheClient;