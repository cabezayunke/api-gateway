'use strict';

var _config = require('config');

var _config2 = _interopRequireDefault(_config);

var _helmet = require('helmet');

var _helmet2 = _interopRequireDefault(_helmet);

var _cors = require('cors');

var _cors2 = _interopRequireDefault(_cors);

var _morgan = require('morgan');

var _morgan2 = _interopRequireDefault(_morgan);

var _bodyParser = require('body-parser');

var _bodyParser2 = _interopRequireDefault(_bodyParser);

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _httpStatus = require('http-status');

var _httpStatus2 = _interopRequireDefault(_httpStatus);

var _httpProxy = require('http-proxy');

var _httpProxy2 = _interopRequireDefault(_httpProxy);

var _router = require('./router');

var _router2 = _interopRequireDefault(_router);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var env = process.env.NODE_ENV || 'production';
console.log('env = ' + env);

// create app
var app = (0, _express2.default)();

// Helmet helps you secure your Express apps by setting various HTTP headers
// https://github.com/helmetjs/helmet
app.use((0, _helmet2.default)());

// Enable CORS with various options
// https://github.com/expressjs/cors
app.use((0, _cors2.default)());

// logging requests
app.use((0, _morgan2.default)('dev'));

// parser
app.use(_bodyParser2.default.json());
app.use(_bodyParser2.default.urlencoded({ extended: false }));

/**
 * Routes
 */

app.use('/api', _router2.default);

// error handler
// stacktrace only in development
app.use(function (err, req, res) {
    console.error(err);
    res.status(err.status || _httpStatus2.default.INTERNAL_SERVER_ERROR);

    var error = {};
    if (env === 'development') {
        error = err;
    }

    res.json({
        message: err.message,
        error: error
    });
});

/**
 * Proxy
 */
// const apiProxy = HttpProxy.createProxyServer();
//
// app.use('/api/blog/*', function(req, res){
//     apiProxy.web(req, res, { target: config.get('api.blog-api') });
// });
// app.use('/api/comments*', function(req, res){
//     apiProxy.web(req, res, { target: config.get('api.comments-api') });
// });
// app.use('/api/tags/*', function(req, res){
//     apiProxy.web(req, res, { target: config.get('api.tags-api') });
// });
// app.use('/api/users/*', function(req, res){
//     apiProxy.web(req, res, { target: config.get('api.users-api') });
// });

// error handler
// apiProxy.on('error', function (err, req, res, next) {
//     return err;
//     res.status(err.status || HttpStatus.INTERNAL_SERVER_ERROR);
//
//     let error = {};
//     if (env === 'development') {
//         error = err;
//     }
//     console.error(err);
//
//     res.json({
//         message: err.message,
//         error: error
//     });
// });

// stacktrace only in development
// app.use(function(err, req, res) {
//
// });


/**
 * Start server
 */
var port = _config2.default.get('node.port') || 3000;
var gateway = _config2.default.get('node.gateway') || "127.0.0.1";

app.listen(port, gateway, function () {
    console.log('API Gateway server listening to ' + gateway + ' on port ' + port);
});

module.exports = app; // for testing