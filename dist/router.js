'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _express = require('express');

var _BlogPostController = require('./controller/BlogPostController');

var _BlogPostController2 = _interopRequireDefault(_BlogPostController);

var _CommentsController = require('./controller/CommentsController');

var _CommentsController2 = _interopRequireDefault(_CommentsController);

var _TagsController = require('./controller/TagsController');

var _TagsController2 = _interopRequireDefault(_TagsController);

var _UsersController = require('./controller/UsersController');

var _UsersController2 = _interopRequireDefault(_UsersController);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var router = new _express.Router();

/*
 * Blog routes
 */
var blogController = new _BlogPostController2.default();

router.get('/blog/posts/:id', blogController.getBlogPostById);
router.get('/blog/posts', blogController.forwardRequest);
router.post('/blog/posts', blogController.forwardRequest);
router.delete('/blog/posts/:id', blogController.forwardRequest);
router.put('/blog/posts/:id', blogController.forwardRequest);

/*
 * Comments API routes
 */
var commentsController = new _CommentsController2.default();

router.get('/comments/:id', commentsController.forwardRequest);
router.get('/comments', commentsController.forwardRequest);
router.post('/comments', commentsController.forwardRequest);
router.delete('/comments/:id', commentsController.forwardRequest);
router.put('/comments/:id', commentsController.forwardRequest);

/*
 * Tags API routes
 */
var tagsController = new _TagsController2.default();

router.get('/tags/:id', tagsController.forwardRequest);
router.get('/tags', tagsController.forwardRequest);
router.post('/tags', tagsController.forwardRequest);
router.delete('/tags/:id', tagsController.forwardRequest);
router.put('/tags/:id', tagsController.forwardRequest);

/*
 * Users API routes
 */
var usersController = new _UsersController2.default();

router.get('/users/:id', usersController.forwardRequest);
router.get('/users', usersController.forwardRequest);
router.post('/users', usersController.forwardRequest);
router.delete('/users/:id', usersController.forwardRequest);
router.put('/users/:id', usersController.forwardRequest);

exports.default = router;