import { Router } from 'express';
import BlogPostController from './controller/BlogPostController';
import CommentsController from './controller/CommentsController';
import TagsController from './controller/TagsController';
import UsersController from './controller/UsersController';

const router = new Router();

/*
 * Blog routes
 */
const blogController = new BlogPostController();

router.get('/blog/posts/:id', blogController.getBlogPostById.bind(blogController));
router.get('/blog/posts', blogController.forwardRequest.bind(blogController));
router.post('/blog/posts', blogController.forwardRequest.bind(blogController));
router.delete('/blog/posts/:id', blogController.forwardRequest.bind(blogController));
router.put('/blog/posts/:id', blogController.forwardRequest.bind(blogController));

/*
 * Comments API routes
 */
const commentsController = new CommentsController();

router.get('/comments/:id', commentsController.forwardRequest.bind(commentsController));
router.get('/comments', commentsController.forwardRequest.bind(commentsController));
router.post('/comments', commentsController.forwardRequest.bind(commentsController));
router.delete('/comments/:id', commentsController.forwardRequest.bind(commentsController));
router.put('/comments/:id', commentsController.forwardRequest.bind(commentsController));

/*
 * Tags API routes
 */
const tagsController = new TagsController();

router.get('/tags/:id', tagsController.forwardRequest.bind(tagsController));
router.get('/tags', tagsController.forwardRequest.bind(tagsController));
router.post('/tags', tagsController.forwardRequest.bind(tagsController));
router.delete('/tags/:id', tagsController.forwardRequest.bind(tagsController));
router.put('/tags/:id', tagsController.forwardRequest.bind(tagsController));

/*
 * Users API routes
 */
const usersController = new UsersController();

router.get('/users/:id', usersController.forwardRequest.bind(usersController));
router.get('/users', usersController.forwardRequest.bind(usersController));
router.post('/users', usersController.forwardRequest.bind(usersController));
router.delete('/users/:id', usersController.forwardRequest.bind(usersController));
router.put('/users/:id', usersController.forwardRequest.bind(usersController));

export default router;