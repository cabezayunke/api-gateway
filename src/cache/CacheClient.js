/**
 * Cache client
 * TODO: implement with Redis
 */
export default class CacheClient {

    /**
     * Gets item from cache
     *
     * @param key
     * @returns {boolean}
     */
    getCachedItem(key) {
        return false;
    }

    /**
     * Saves item to cache
     *
     * @param key
     * @param data
     * @returns {boolean}
     */
    saveItem(key, data) {
        return false;
    }
}