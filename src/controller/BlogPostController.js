import config from 'config';
import ApiGatewayController from './ApiGatewayController';

/**
 * Blog post controller
 * (will be used by the router)
 */
export default class BlogPostController extends ApiGatewayController {

    /**
     * Constructor
     * (sets the correct api url)
     */
    constructor() {
        super(config.get('api.blog'))
    }

    /*
     * Custom controller methods here
     */

    /**
     * Get blog post by ID
     * Aggregates data from tags and comments
     * so the client gets all the data in one request
     *
     * @param req
     * @param res
     * @param next
     */
    getBlogPostById(req, res, next) {
        //TODO:
        // make multiple requests here
        // to blog, tags and comments APIs
        // and return a single object
        // Axios allows to send multiple requests:
        //
        // function getUserAccount() {
        //     return axios.get('/user/12345');
        // }
        //
        // function getUserPermissions() {
        //     return axios.get('/user/12345/permissions');
        // }
        //
        // axios.all([getUserAccount(), getUserPermissions()])
        //     .then(axios.spread(function (acct, perms) {
        //         // Both requests are now complete
        //     }));
        this.forwardRequest(req, res, next);
    };

    /*
     * END of custom controller methods
     */
}