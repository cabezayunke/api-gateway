import config from 'config';
import ApiGatewayController from './ApiGatewayController';

/**
 * Comments controller
 * (will be used by the router)
 */
export default class CommentsController extends ApiGatewayController {

    /**
     * Constructor
     * (sets the correct api url)
     */
    constructor() {
        super(config.get('api.comments'))
    }

}