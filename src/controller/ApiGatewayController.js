import axios from 'axios';
import CacheClient from '../cache/CacheClient';

/**
 * Api gateway controller
 */
export default class ApiGatewayController {

    /**
     * Constructor
     * @param baseUrl, set from child class
     */
    constructor(baseUrl) {
        console.log('setting baseUrl ' + baseUrl);
        this.baseUrl = baseUrl;
        this.cacheClient = new CacheClient();
    }

    /**
     * Forward request to corresponding service
     *
     * @param req
     * @param res
     * @param next
     */
    forwardRequest(req, res, next) {
        console.log("forwardRequest to " + this.baseUrl);
        // prepare options
        let options = {
            method: req.method || 'get',
            url: this.baseUrl + req.originalUrl,
            responseType: 'application/json'
        };
        if(req.method === 'POST' || req.method === 'PUT') {
            options.data = req.body || {};
        }
        console.log(options);

        // check cache
        if(req.method === 'get') {
            let cachedItem = this.cacheClient.getCachedItem(options.url);
            if(cachedItem) {
                res.status(apiResponse.status).send(apiResponse.data);
                return;
            }
        }

        // send request
        axios(options)
        .then((apiResponse) => {
            console.log(apiResponse.data);
            // save item to cache
            if(req.method === 'get') {
                this.cacheClient.saveItem(options.url, apiResponse.data);
            }
            res.status(apiResponse.status).send(apiResponse.data);
        })
        .catch((error) => {
            if (error.response) {
                // The request was made and the server responded with a status code
                // that falls out of the range of 2xx
                res.status(error.response.status || 500).send(error.response.data || '');
            } else if (error.request) {
                // The request was made but no response was received
                // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
                // http.ClientRequest in node.js
                console.log(error.request);
                next(error.request);
            } else {
                // Something happened in setting up the request that triggered an Error
                console.warn(error);
                next(error);
            }
        });

    }

}