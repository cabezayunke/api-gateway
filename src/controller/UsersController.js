import config from 'config';
import ApiGatewayController from './ApiGatewayController';

/**
 * Users controller
 * (will be used by the router)
 */
export default class UsersController extends ApiGatewayController {

    /**
     * Constructor
     * (sets the correct api url)
     */
    constructor() {
        super(config.get('api.users'))
    }

}