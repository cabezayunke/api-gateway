import config from 'config';
import ApiGatewayController from './ApiGatewayController';

/**
 * Tags controller
 * (will be used by the router)
 */
export default class TagsController extends ApiGatewayController {

    /**
     * Constructor
     * (sets the correct api url)
     */
    constructor() {
        super(config.get('api.tags'))
    }

}